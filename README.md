# ISAAC

The intelligent self-organizing agent-based aggregator and controller for power systems.

## Installing Python

Under Ubuntu:

    sudo apt install python3.4-dev python3.5-dev python3.6-dev

## Bootstrapping tox

Under Ubuntu:

    sudo apt install python-tox

Under Windows:

    python -m pip install tox

## Compatibility Testing

    tox

## Activating the Virtual Environment

Under Linux:

    source .tox/py36/bin/activate

Under Windows:

    .tox/py36/Scripts/activate.bat

## Using

    openvpp-mosaik

    openvpp-container

## Deactivating the Virtual Environment

    deactivate
