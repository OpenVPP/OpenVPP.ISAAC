import aiomas


class TestingAgent(aiomas.Agent):
    def __init__(self, container, name):
        super().__init__(container)
        self._name = name

    @aiomas.expose
    def method(self, array):
        return (self._name, array)

    @aiomas.expose
    def get_time(self):
        return self.container.clock.time()
